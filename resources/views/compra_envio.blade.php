@extends('layout.app')
@section('content')
<div id="content" class="container">
    <div class="row justify-content-center">
<div class="col-md-8">
<div class="card">
  <div class="card-header">Registro</div>

  <div class="card-body">
    <form method="POST" action="" enctype="multipart/form-data">
      <input type="hidden" name="_token" value="dJ5XKrDQ3rJs1QwRrAQ1DH1Tp7KuGPWulhGbm0l3">
      <div class="form-group row">
        <label for="name" class="col-md-4 col-form-label text-md-right">Nombre</label>
        <div class="col-md-6">
          <input id="name" type="text" class="form-control" name="name" value="Pablo Motos" autocomplete="name" autofocus="">
        </div>
      </div>

      <div class="form-group row">
        <label for="email" class="col-md-4 col-form-label text-md-right">email</label>

        <div class="col-md-6">
          <input id="email" type="email" class="form-control" name="email" value="pmotos@example.com" autocomplete="email">
        </div>
      </div>

      <div class="form-group row">
        <label for="direction" class="col-md-4 col-form-label text-md-right">Direccion</label>

        <div class="col-md-6">
          <input id="direction" type="text" class="form-control" name="direction">
        </div>
      </div>

      <div class="form-group row">
        <label for="password" class="col-md-4 col-form-label text-md-right">Contraseña</label>

        <div class="col-md-6">
          <input id="password" type="password" class="form-control" name="password">
        </div>
      </div>

      <div class="form-group row">
        <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Confirmar contraseña</label>

        <div class="col-md-6">
          <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
        </div>
      </div>

      <div class="form-group row">
        <label for="foto" class="col-md-4 col-form-label text-md-right">Foto</label>

        <div class="col-md-6">
          <input id="photo" type="file" class="form-control" name="photo">
        </div>
      </div>
      <div class="form-group row">
        <div class="col-md-6">
          <input id="name" type="hidden" class="form-control is-invalid" name="name" value="Thalia Ernser" autocomplete="name" autofocus="">
          <span class="invalid-feedback" role="alert">
                          </span>
        </div>
      </div>
      <div class="form-group row mb-0">
        <div class="col-md-6 offset-md-4">
          <!-- <button type="submit" class="btn btn-primary">
            Pagar
          </button> -->
        </div>
      </div>
    </form>
  </div>
</div>
</div>
</div>
<a href="{{ url()->previous() }}" class="btn btn-secondary btn-lg float-left">Atras</a>
<a href="/compra/confirmar"  class="btn btn-primary btn-lg float-right">Siguiente</a>

<br><br>
@endsection
