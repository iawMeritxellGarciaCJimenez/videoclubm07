@extends("layout.app")
<div class="container" style="height:100px;width:100%;"></div><!--Space between header-->
    <div class="container mb-5">
        <ul class="nav w-25 float-start" style="margin-top:">
            <h2>Buscar (en este genero)</h2>
            <li>
                <form action="/search"  method="get" enctype="multipart/form-data" class="d-flex justify-content">
                    <input class="form-control" type="text" name="film">
                </form>
            </li>
            <ul class="list-inline">
                <h2 class="w-100">Genero</h2>
                <li class="mb-4">
                    <a href="/category/1">Fantasia</a>
                </li>
                <li class="mb-4">
                    <a href="/category/2">Accion</a>
                </li>
                <li class="mb-4">
                    <a href="/category/3">Romance</a>
                </li>
            </ul>
        </ul>
@section("content")

<div id="myCarousel" style="width: 575px; height: 800px;" class="carousel slide" data-bs-ride="carousel">
    <div class="carousel-indicators">
        @php
            $dataBs = 1;
        @endphp
        @foreach ($movies as $movie)
            @if ($loop->first)
                <button type="button" data-bs-target="#myCarousel" data-bs-slide-to="0" class="active" aria-current="true"></button>
                @php
                $i = "isntFirst"
                @endphp
            @else 
                <button type="button" data-bs-target="#myCarousel" data-bs-slide-to="{{$dataBs}}"></button>
                @php
                    $dataBs++;   
                @endphp
            @endif
        @endforeach
    </div>
    <div class="carousel-inner">
        @forelse ($movies as $movie)
            @if ($loop->first)
                <div class="carousel-item active">
                    <div class="container">
                        <img src="/img/{{$movie->cover}}" height="800px" width="575px">
                    </div>
                </div>
            @else 
                <div class="carousel-item">
                    <div class="container">
                        <img src="/img/{{$movie->cover}}" height="800px" width="575px">
                    </div>
                </div>
            @endif
        @empty
            <p>No movies</p>
        @endforelse
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#myCarousel" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#myCarousel" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
    </button>
</div>
<div class="container">
    @forelse ($movies as $movie)
        <div class="float-start m-4 p-2">
            <img class="mx-auto rounded" src="img/{{$movie->cover}}" height="300" width="200rem">
            <p class="text-wrap">{{$movie->title}}</p>
            <p class="text-wrap" >{{$movie->price}}€</p>
            <p class="text-wrap">{{$movie->description}}</p>


            <form method="post" action={{ route('addToChart') }}>
                @csrf
                <input type="hidden" name="idMovie" value="{{$movie->id}}">
                <input type="hidden" name="titleMovie" value="{{$movie->title}}">
                <input type="hidden" name="coverMovie" value="{{$movie->cover}}">
                <input type="hidden" name="priceMovie" value="{{$movie->price}}">
                <button type="submit" name="comprarMovie" value="comprar" class="btn btn-primary w-100 mb-2">Comprar</button>
                <button type="submit" name="alquilarMovie" value="alquilar" class="btn btn-primary w-100">Alquilar</button>
            </form>

        </div>
    @empty 
        <p>No movies</p>
    @endforelse
</div>
@endsection