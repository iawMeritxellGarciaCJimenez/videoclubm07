<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Netflis</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    </head>
    <body>
        <header class="fixed-top d-flex p-3 bg-dark text-white flex-wrap align-items-center justify-content-center justify-content-md-between py-3 mb-4 p-4 border-bottom">
            <a href="/" style="color: #fff;" class="navbar-brand">Netflis</a>
            <ul class="nav d-flex justify-content-end">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-bs-toggle="dropdown" aria-expanded="false">Numero de produtos: {{sizeof(app('request')->session()->get('carrito',[]))}}</a>
                        <ul class="dropdown-menu" aria-labelledby="dropdown01">
                           @forelse (app('request')->session()->get('carrito',[]) as $movie)
                             <li>
                               <a class="dropdown-item" href="#">{{$movie->title}} ({{$movie->comprar}}{{$movie->alquilar}})</a>
                             </li>
                           @empty
                             <li>
                               <a class="dropdown-item" href="#">No hay productos en la cesta </a>
                             </li>
                           @endforelse
                        </ul>
                      </li>
                      @if(app('request')->session()->has('carrito'))
                         <li class="nav-item">
                             <a class="nav-link text-white" type="text" class="btn btn-primary" href={{ route('emptyChart') }}>Borrar Carrito</a>
                        </li>
                      @endif
                    <li>
                        <a href="/compra/resumen" class="nav-link px-2 text-white">Finalizar</a>
                    </li>
            </ul>
        </header>
        <div class="container" style="height:100px;width:100%;"></div><!--Space between header-->
            <main class="float-start w-50 " style="margin-bottom:200px">
                @yield('content')
            </main>
        </div>
        <footer class="w-100 mt-4 text-center bg-dark text-white text-lg-start" style="position: fixed;bottom:0px">
            <div class="text-center p-4">
            Copyright &#169; Meritxell
            </div>
        </footer>
    </body>
</html>