@extends('layout.app')
@section('content')
    @forelse ($movies as $movie)
    <div class="float-start m-2 p-2">
        <img class="mx-auto rounded" src="/img/{{$movie->cover}}" height="300" width="200rem">
        <p class="text-wrap">{{$movie->title}}</p>
        <p class="text-wrap" >{{$movie->price}}€</p>
        <p class="text-wrap">{{$movie->description}}</p>

        <form method="post" action={{ route('addToChart') }}>
            @csrf
            <input type="hidden" name="idMovie" value="{{$movie->id}}">
            <input type="hidden" name="titleMovie" value="{{$movie->title}}">
            <input type="hidden" name="coverMovie" value="{{$movie->cover}}">
            <input type="hidden" name="priceMovie" value="{{$movie->price}}">
            <button type="submit" name="comprarMovie" value="comprar" class="btn btn-primary w-100 mb-2">Comprar</button>
            <button type="submit" name="alquilarMovie" value="alquilar" class="btn btn-primary w-100">Alquilar</button>
        </form>
    </div>
    @empty
        <p>No category</p>
    @endforelse 
@endsection