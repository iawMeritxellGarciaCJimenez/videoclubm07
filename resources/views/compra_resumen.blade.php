@extends('layout.app')
<div class="container" style="height:100px;width:100%;"></div>
@section('content')
<div id="content" class="container">
    <div class="row">
        <div class="col-lg-3">
        <h1 class="my-4">Resumen de compra</h1>
        <ul class="list-group">
        <li class="list-group-item">
            Total de productos: 2</li>
        <li class="list-group-item">
            Precio total de productos: 24</li>
        </ul>
        </div>
        <div class="col-lg-9">
        @forelse (app('request')->session()->get('carrito',[]) as $movie)
        <div class="container">

            <img class="mx-auto rounded" src="/img/{{$movie->cover}}" height="400" width="300rem">
            <ul class="list-inline">
                <li>
                    <a class="dropdown-item" href="#">{{$movie->title}}</a>
                </li>
                <li>
                    <a class="dropdown-item" href="#">{{$movie->comprar}}</a>
                    <a class="dropdown-item" href="#">{{$movie->alquilar}}</a>
                </li>
                <li>
                    <a class="dropdown-item" href="#">{{$movie->price}}€</a>
                </li>
            </ul>
        </div>
        @empty
            <li>
            <a class="dropdown-item" href="#">No hay productos en la cesta </a>
            </li>
        @endforelse

        <a href="{{ url()->previous() }}" class="btn btn-secondary btn-lg float-left">Atras</a>
        <a href="/compra/envio" class="btn btn-primary btn-lg float-right">Siguiente</a>
        <br><br>
        </div>
    </div>
</div>
@endsection