<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use \Carbon\Carbon;

class MovieFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->name(),
            'price' => $this->faker->numerify('##.##'),
            'cover' => str_replace(' ','',$this->faker->name().'.jpg'),
            'file' => str_replace(' ','',$this->faker->name().'.mp4'),
            'description' => $this->faker->paragraph,
            'created_at' => Carbon::now(),
        ];
    }
}
