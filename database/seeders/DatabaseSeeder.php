<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            MovieSeeder::class,
            GenresSeeder::class,
            MovieXGenresSeeder::class,
        ]);
    }
}
/*
./vendor/bin/sail php artisan tinker
DB::table('movies')->get();
*/