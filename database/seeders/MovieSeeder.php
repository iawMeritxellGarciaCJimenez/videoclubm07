<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class MovieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //\App\Models\Movie::factory(10)->create();//En caso de usar la MovieFactory
        DB::table('movies')->insert([
            ['title' => 'Señor de los anillos 1','description' => 'Señor de los anillos 1 description','file'=>'señor_anillos_1.mp4','cover'=>'señor_anillos_1.jpeg','price' => mt_rand(0,60)],
            ['title' => 'Señor de los anillos 2','description' => 'Señor de los anillos 2 description','file'=>'señor_anillos_2.mp4','cover'=>'señor_anillos_2.jpg','price' => mt_rand(0,60)],
            ['title' => 'Iron man 1', 'description' => 'Iron man 1 de marvel','file'=>'iron_man_1.mp4','cover'=>'iron_man_1.webp','price' => mt_rand(0,60)],
            ['title' => 'Passengers','description' => 'Passengers romance film','file'=>'passengers.mp4','cover'=>'passengers.jpg','price' => mt_rand(0,60)],
        ]);
    }
}
