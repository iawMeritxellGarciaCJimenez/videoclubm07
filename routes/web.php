<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ListaPeliculasController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[ListaPeliculasController::class,'list']);
Route::get('/search/filter-name',[ListaPeliculasController::class,'filtroTitulo']);
Route::get('/category/{id}',[ListaPeliculasController::class,'filtroGeneros']);
Route::get('/search',[ListaPeliculasController::class,'filtroTitulo']);


Route::post('/products/addToChart', [ListaPeliculasController::class, 'addToChart'])->name('addToChart');
Route::get('/products/emptyChart', [ListaPeliculasController::class, 'emptyChart'])->name('emptyChart');

Route::get('/compra/resumen', function () {
    return view('compra_resumen');
});

Route::get('/compra/envio', function () {
    return view('compra_envio');
});
Route::get('/compra/confirmar', function () {
    return view('confirmar_compra');
});
Route::get('/compra/end', function () {
    return view('compra_finalizada');
});