<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Movie;
class ListaPeliculasController extends Controller
{
    private $movieModel;
    public function __construct()
    {
        $this->movieModel = new Movie();
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list() {
        $movies = $this->movieModel;
        return view('/home')->with('movies', $movies->get());
    }
    public function filtroGeneros(Request $genre) {
        $movies = $this->movieModel->joinGenres($genre);
        $movies->genre($genre->route()->id);
        return view('/category')->with('movies', $movies->get());
    }
    public function filtroTitulo(Request $searchMovie) {
        $searchMovie->flash();
        $movies = $this->movieModel->query();
        
        if ($searchMovie->filled('film')) {
            $movies->title($searchMovie->input('film'));
        }
        
        return view('search')->with('movies', $movies->get());
    }
    public function addToChart(Request $request)
    {
        $carrito = $request->session()->get('carrito', []);
        $actual = (object) array('id'=>$request->input('idMovie'),'title'=>$request->input('titleMovie'),
        'cover'=>$request->input('coverMovie'),
        'price'=>$request->input('priceMovie'),'alquilar'=>$request->input('alquilarMovie'),'comprar'=>$request->input('comprarMovie'));
        array_push($carrito, $actual);
        $request->session()->put('carrito', $carrito);

        //Redirect to page who send the request:
        return redirect(url()->previous());
    }

    public function emptyChart(Request $request)
    {
        $request->session()->forget('carrito');
        return redirect()->back();
    }

}
