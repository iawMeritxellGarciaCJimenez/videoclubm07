<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    public function scopeJoinGenres($query){
        // multi genre
        return $query->join('movies_x_genres', 'movies.id', 'id_movie')
            ->join('genres', 'id_genre', 'genres.id');

    }
    // Only one genre
    public function scopeGenre($query,$input) {
        return $query->where('genres.id',$input);
    }
    // Multi genre
    public function scopeGenreIn($query, $input) {
        return $query->whereIn('genres.genre',$input);
	}
    public function scopeTitle($query, $title) {
        return $query->where('title','like','%' . $title  .'%');
	}
    public function scopeId($query, $id) {
        return $query->where('id',$id);
	}
}
